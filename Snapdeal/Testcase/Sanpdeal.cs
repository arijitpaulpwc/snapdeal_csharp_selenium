﻿using Snapdeal.ReusableMethods;
using Programm.Pages;
using Programm.ReusableMethods;
using Snapdeal.Pages;

namespace Snapdeal.Testcase
{
    [TestFixture]
    class Snapdeal : Reusable
    {
        HomePage homePage = null;
        Headphone headphone = null;
        Excel dataSet = null;
        ReportGenerate reportGenerate = null;
        [SetUp]
        public void setUP()
        {
            Reader reader = new Reader();
            string url = reader.getProperty("URL");
            string dataSetPath = reader.getProperty("ExcelPath");
            //string resultPath1 = reader.getProperty("ResultPath");
            dataSet = new Excel(dataSetPath);
            launchApp(url);
            homePage = new HomePage();
            headphone = new Headphone();    
            reportGenerate = new ReportGenerate();
            string testCaseName = getTestCaseName(this.GetType().Name);
            reportGenerate.testCaseName = testCaseName;
            reportGenerate.createReport();
        }
        [Test]

        public void execution()
        {
            homePage.verifyHomePageIsDisplayed();
            homePage.verifyCategoriesIsDisplayed();
            homePage.clickElectronisCategories();
            homePage.clickHeadphoneSubCategories();
            headphone.selectPriceLowToHigh();
            headphone.verifyFirstProductNameAndPrice();
            headphone.clickOnFirstProduct();
            headphone.clickOnBuyNow();
            headphone.verifyLoginBoxIsDisplayed();
        }

        [TearDown]
        public void tearDown()
        {
            reportGenerate.flushReport();
            closeBroswer();
        }
    }
}
