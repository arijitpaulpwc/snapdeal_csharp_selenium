﻿using AventStack.ExtentReports;
using OpenQA.Selenium;
using Snapdeal.ReusableMethods;

namespace Snapdeal.Pages
{
    class Headphone : Reusable
    {
        By sortByDropdown = By.XPath("//*[@class=\"sort-drop clearfix\"]");
        By priceLowToHigh = By.XPath("//*[@data-sorttype=\"plth\"]");
        By firstProductName = By.XPath("(//*[@class=\"product-title\"])[1]");
        By firstProductPrice = By.XPath("(//*[@class=\"lfloat product-price\"])[1]");
        By BuyNowButton = By.XPath("//*[@id=\"buy-button-id\"]");
        By loginBox = By.XPath("//*[@id=\"checkout-login\"]");

        ReportGenerate reportGenerate = new ReportGenerate();

        public void clickOnSortByDropDown()
        {
            clickOn(sortByDropdown);
            reportGenerate.updateTestStep("Clicked on the SortBy DropDown", Status.Pass);
        }
        public void clickOnPriceLowToHigh()
        {
            clickOn(priceLowToHigh);
            reportGenerate.updateTestStep("Clicked on the Price Low To High Option", Status.Pass);
        }
        public void selectPriceLowToHigh()
        {
            clickOnSortByDropDown();
            clickOnPriceLowToHigh();
        }
        public void verifyFirstProductName()
        {
            scrollIntoElement(firstProductName);
            waitForElement(firstProductName);
            string name_firstProduct = driver.FindElement(firstProductName).Text;
            reportGenerate.updateTestStep("First Product Name is Displayed : "+ name_firstProduct.Split("Assorted")[0], Status.Info);
        }
        public void verifyFirstProductPrice()
        {
            waitForElement(firstProductPrice);
            string price_firstProduct = driver.FindElement(firstProductPrice).Text;
            reportGenerate.updateTestStep("First Product Price is Displayed : " + price_firstProduct, Status.Info);
        }
        public void verifyFirstProductNameAndPrice()
        {
            verifyFirstProductName();
            verifyFirstProductPrice();
        }
        public void clickOnFirstProduct()
        {
            clickOn(firstProductName);
            reportGenerate.updateTestStep("Clicked on the First Product", Status.Pass);
        }
        public void clickOnBuyNow()
        {
            switchToTab();
            clickOn(BuyNowButton);
            reportGenerate.updateTestStep("Clicked on the Buy Now Button", Status.Pass);
        }
        public void switchToTab()
        {
            List<string> browserTabs = driver.WindowHandles.ToList();
            driver.SwitchTo().Window(browserTabs[1]);
            reportGenerate.updateTestStep("Switched to the Selected Headphone Tab", Status.Info);
            ///////
            //foreach(var a in browserTabs)
            //{
            //    reportGenerate.updateTestStep("Tab : "+ a, Status.Info);
            //}
        }
        public void verifyLoginBoxIsDisplayed()
        {
            waitForElement(loginBox);
            reportGenerate.updateTestStep("Login Page is displayed", Status.Pass);
        }
    }
}
