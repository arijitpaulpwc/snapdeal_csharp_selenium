﻿using Snapdeal.ReusableMethods;
using OpenQA.Selenium;
using AventStack.ExtentReports;

namespace Programm.Pages
{
    public class HomePage : Reusable
    {   
        By electronics = By.XPath("//*[@class=\"leftHead\"]/following-sibling::li/a/span[contains(text(),\"Electronics\")]");
        By headPhoneSubCategories = By.XPath("//span[contains(text(),\"Headphones\")]");
        By homePageXpath = By.XPath("//*[@data-pagename=\"HomePage\"]");
        By categoriesXpath = By.XPath("//*[@class=\"leftNavigationLeftContainer expandDiv\"]");

        ReportGenerate reportGenerate = new ReportGenerate();


        public void clickElectronisCategories()
        {
            clickOn(electronics);
            reportGenerate.updateTestStep("Clicked on the Electronics Categories", Status.Pass);
        }
        public void clickHeadphoneSubCategories()
        {
            clickOn(headPhoneSubCategories);
            reportGenerate.updateTestStep("Clicked on the Headphone SubCategories", Status.Pass);
        }
        public void verifyHomePageIsDisplayed()
        {
            waitForElement(homePageXpath);
            reportGenerate.updateTestStep("HomePage is Displayed", Status.Pass);
        }
        public void verifyCategoriesIsDisplayed()
        {
            waitForElement(categoriesXpath);
            reportGenerate.updateTestStep("Categories are present at the Homepage", Status.Pass);
        }
    }
}
