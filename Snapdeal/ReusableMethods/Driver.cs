﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Snapdeal.ReusableMethods
{
    public class Driver
    {
        public static WebDriver driver = null;

        public static void initiateBrowser()
        {
            WebDriver chromeDriver = new ChromeDriver();
            driver = chromeDriver;
        }
        public static void navigateTo(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public static void launchApp(string url)
        {
            initiateBrowser();
            navigateTo(url);
            driver.Manage().Window.Maximize();
        }

        public static void closeBroswer()
        {
            driver.Quit();
        }
    }
}
