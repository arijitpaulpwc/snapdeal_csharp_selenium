﻿
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using OpenQA.Selenium;

namespace Snapdeal.ReusableMethods
{
    public class ReportGenerate : Reusable
    {
        static string resultPath = new Reader().getProperty("ResultPath");
        public string testCaseName = "";
        static ExtentTest extentTest = null;
        static string currentTimeMillis = Convert.ToString(CurrentTimeMillis());
        //static string resultPath = "C:\\Snapshotviews\\c# Learning\\Snapdeal\\Snapdeal\\Report\\";
        string latestRunFolderPath = getResultFolderName() + "\\Run_" + currentTimeMillis;

        public string testCaseResultFolderPath = "path";
        ExtentReports extent = new ExtentReports();
        public ReportGenerate()
        {
        }
        private static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public static long CurrentTimeMillis()
        {
            return (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
        }
        public string getTestCaseResultFolderPath()
        {
            return testCaseResultFolderPath;
        }

        public void setTestCaseResultFolderPath(string testCaseResultFolderPath)
        {
            this.testCaseResultFolderPath = testCaseResultFolderPath;
        }

        public ExtentTest createTest(string testCaseName)
        {
            extentTest = extent.CreateTest(testCaseName);
            return extentTest;
        }
        public void updateTestStep(string description, AventStack.ExtentReports.Status status)
        {
            extentTest.Log(status, description);
        }
        public void createReport()
        {
            createDirectoryForTestCaseInsideLatestRunDirectory();
            testCaseResultFolderPath = getTestCaseResultFolderPath();
            ExtentSparkReporter spark = new ExtentSparkReporter(testCaseResultFolderPath + "/report.html");
            extent.AttachReporter(spark);
            createTest(testCaseName);
        }
        public void flushReport()
        {
            extent.Flush();
            extentTest = null;
        }
        public void createLatestRunDirectory()
        {
            if (!Directory.Exists(latestRunFolderPath))
            {
                Directory.CreateDirectory(latestRunFolderPath);
            }
        }
        public void createDirectoryForTestCaseInsideLatestRunDirectory()
        {
            createLatestRunDirectory();
            string newDirectory = latestRunFolderPath + "\\" + testCaseName;
            if (!Directory.Exists(newDirectory))
            {
                Directory.CreateDirectory(newDirectory);
            }
            setTestCaseResultFolderPath(latestRunFolderPath + "\\" + testCaseName);
        }

        public static string getResultFolderName()
        {
            string fileName1 = DateTime.Now.ToString("dd_MMM_yyyy");
            return resultPath + fileName1;
        }

        public void takeScreenshot(string stepName)
        {
            string path = getTestCaseResultFolderPath() + "\\" + stepName + "_" + CurrentTimeMillis() + "";
            ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile(path + ".png");
        }
        //public String takeScreenshotBase64String() throws IOException
        //{
        //    TakesScreenshot scrShot =((TakesScreenshot)driver);
        //    String base64ScreenshotCode = scrShot.getScreenshotAs(OutputType.BASE64);
        //        return base64ScreenshotCode;
        //}
    }
}
