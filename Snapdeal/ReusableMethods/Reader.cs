﻿namespace Snapdeal.ReusableMethods
{
    public class Reader
    {
        //public Properties readFromPropertiesFile()
        //{
        //    FileReader reader = new FileReader("C:\\Snapshotviews\\Comp\\src\\main\\resources\\TestData.properties");
        //    Properties props = new Properties();
        //    props.Load(reader);
        //    return props;

        //}

        //public string emailIDOfTheTeam(string teamEmail)
        //{
        //    return readFromPropertiesFile().GetProperty(teamEmail);
        //}

        //public string getValue(String key)
        //{
        //    return readFromPropertiesFile().getProperty(key);
        //}
        public Dictionary<string, string> readProperty()
        {
            var data = new Dictionary<string, string>();
            foreach (var row in File.ReadAllLines("C:\\Snapshotviews\\c# Learning\\Snapdeal\\Snapdeal\\Properties.txt"))
                data.Add(row.Split('=')[0], string.Join("=", row.Split('=').Skip(1).ToArray()));
            foreach (var row in data)
                Console.WriteLine(row);
            return data;   
        }
        public string getProperty(string key)
        {
            Dictionary<string, string> data = readProperty();
            return data[key];
        }
    }
}
