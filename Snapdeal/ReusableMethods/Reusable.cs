﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
namespace Snapdeal.ReusableMethods
{
    public class Reusable : Driver
    {
        public void waitForElement(By element)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                wait.Until(ExpectedConditions.ElementToBeClickable(element));
            } catch (NoSuchElementException ex)
            {
                new ReportGenerate().updateTestStep(ex.ToString(), AventStack.ExtentReports.Status.Fail);
            }
            
        }
        public void clickOn(By element)
        {
            waitForElement(element);
            driver.FindElement(element).Click();
        }
        public void scroll(int x, int y)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("javascript:window.scrollBy(" + x + "," + y + ")");
            Thread.Sleep(1000);
        }
        public void scrollIntoElement(By webElement)
        {
            var element = driver.FindElement(webElement);
            Actions actions = new Actions(driver);
            actions.MoveToElement(element);
            actions.Perform();
        }
        public string getTestCaseName(String fullTestCaseName)
        {
            string [] a = fullTestCaseName.Split("\\.");
            string name = a[a.Length - 1];
            return name;
    }
}
}
