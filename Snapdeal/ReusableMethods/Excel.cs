﻿using NUnit.Framework.Internal.Execution;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programm.ReusableMethods
{
    public class Excel
    {
        public string filePath;
        ExcelWorksheet worksheet = null;
        public Excel(string path)
        {
            this.filePath = path;
        }

        public void openExcel()
        {
            FileInfo fi = new FileInfo(filePath);
            ExcelPackage package = new ExcelPackage(fi);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            try {
                Console.WriteLine(package);
                this.worksheet = package.Workbook.Worksheets[0];
                Console.WriteLine(worksheet);
            }
            catch(Exception ex) {
                Console.WriteLine(ex.StackTrace);
            }
        }
        public string readCell(string columnName)
        {
            openExcel();
            //int totalRows = worksheet.Dimension.Rows;
            int totalColumns = worksheet.Dimension.Columns;

            for (int j = 1; j <= totalColumns; j++)
            {
                
                if (worksheet.Cells[1, j].Value.Equals(columnName))
                {
                    return worksheet.Cells[2, j].Value.ToString();
                }
            }
            return "";
        }
        public string[,] readCellData()
        {
            openExcel();
            
            int totalColumns = worksheet.Dimension.Columns;
            int totalRows = worksheet.Dimension.Rows;
            string[,] aa = new string[totalRows+1,totalColumns+1];

            for (int j = 1; j <= totalColumns; j++)
            {
                for(int k= 1; k <= totalRows; k++)
                {
                    aa[j-1,k-1] = worksheet.Cells[k, j].Value.ToString();
                    Console.WriteLine(worksheet.Cells[k, j].Value.ToString());
                }
            }
            Console.WriteLine(aa);
            return aa;
        }

    }
}
